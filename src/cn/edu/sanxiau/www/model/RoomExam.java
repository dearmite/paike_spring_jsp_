package cn.edu.sanxiau.www.model;


public class RoomExam {
	
	private int room_exam_id;
	private int examId;
	private int roomId;
	private String date;
	private String time;
	public int getRoom_exam_id() {
		return room_exam_id;
	}
	public void setRoom_exam_id(int room_exam_id) {
		this.room_exam_id = room_exam_id;
	}
	public int getExamId() {
		return examId;
	}
	public void setExamId(int examId) {
		this.examId = examId;
	}
	public int getRoomId() {
		return roomId;
	}
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	@Override
	public String toString() {
		return "RoomExam [room_exam_id=" + room_exam_id + ", examId=" + examId + ", roomId=" + roomId + ", date=" + date
				+ ", time=" + time + "]";
	}
	
    
}
