package cn.edu.sanxiau.www.model;

public class User2 {
	// 用户ID
	private int userId;
	// 姓名
	private String name;
	// 性别
	private String sex;
	// 地址
	private String adress;
	// 电话
	private String tel;
	// 登录名
	private String userName;
	// 密码
	private String password;

	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "User [userId=" + userId + ", name=" + name + ", sex=" + sex + ", adress=" + adress + ", tel=" + tel
				+ ", userName=" + userName + ", password=" + password + "]";
	}
	
}
