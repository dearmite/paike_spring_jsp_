package cn.edu.sanxiau.www.model;

public class Role {
	// 角色ID
	private int roleId;
	// 名称
	private String name;
	// 状态
	private String state;
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Override
	public String toString() {
		return "Role [roleId=" + roleId + ", name=" + name + ", state=" + state + "]";
	}

	
}
