package cn.edu.sanxiau.www.model;


public class RoomCourse {
	
	private int room_course_Id;
	private int courseId;
	private int roomId;
	private String date;
	private String time;
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
	public int getRoom_course_Id() {
		return room_course_Id;
	}
	public void setRoom_course_Id(int room_course_Id) {
		this.room_course_Id = room_course_Id;
	}
	public int getCourseId() {
		return courseId;
	}
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}
	
	public int getRoomId() {
		return roomId;
	}
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "RoomCourse [room_course_Id=" + room_course_Id + ", courseId=" + courseId + ", roomId=" + roomId
				+ ", date=" + date + "]";
	}

}