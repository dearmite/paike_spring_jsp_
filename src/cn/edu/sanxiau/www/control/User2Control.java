package cn.edu.sanxiau.www.control;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.edu.sanxiau.www.model.User2;
import frame.utils.control.BaseControl2;
import frame.utils.model.PageBean;

public class User2Control extends BaseControl2 {

	/**
	 * <p>
	 * 得到当前页码（page code）<br>
	 * 如果pc参数不存在，说明pc=1<br>
	 * 如果pc参数存在，需要转化int类型<br>
	 * </p>
	 * 
	 * @author zlf
	 * @Date 2019年7月12日
	 * @param req
	 * @param resp
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public int getPC(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String value = req.getParameter("pc");
		if (value == null || value.trim().isEmpty()) {
			return 1;
		}
		return Integer.parseInt(value);

	}

	public String listPage(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// 1、接受数据
		System.out.println("------------listPage()---");
		// a、 获取页面传递的pc:当前页码（page code）
		int pc = getPC(req, resp);
		// b、给定ps的值:每页记录数（page size）
		int ps = 5;

		// 3、调用业务
		PageBean<User2> pageBean = user2Service.queryAllPage(pc, ps);
		req.setAttribute("pb", pageBean);

		// 2、跳转页面
		return "view/system/user2/user2List.jsp";
	}
}
