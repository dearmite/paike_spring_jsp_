package cn.edu.sanxiau.www.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.edu.sanxiau.www.model.Course;
import cn.edu.sanxiau.www.model.PaiKe;
import cn.edu.sanxiau.www.model.RoomCourse;
import frame.utils.control.BaseControl2;

public class PaikeControl extends BaseControl2 {
	public String list(HttpServletRequest req, HttpServletResponse resp) {

		List<PaiKe> paikeList_db_list = courseService.queryPkBycourseId();

		req.setAttribute("paikeList_db_list", paikeList_db_list);
		// 2、跳转页面
		return "view/system/paike/pakeList.jsp";
	}

	public int getPC(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String value = req.getParameter("pc");
		if (value == null || value.trim().isEmpty()) {
			return 1;
		}
		return Integer.parseInt(value);

	}

	public String listStu(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// 3、调用业务

		List<PaiKe> paikeList_db_list = courseService.queryPkBycourseId();

		req.setAttribute("paikeList_db_list", paikeList_db_list);
		// 2、跳转页面
		return "view/system/paike/pakeListStu.jsp";
	}

	public String addUI(HttpServletRequest req, HttpServletResponse resp) {

		List<PaiKe> classRoom_db_list = classRoomService.queryAllCourse();
		List<Course> course_db_list = courseService.queryAllCourseByState("0");
		req.setAttribute("classRoom_db_list", classRoom_db_list);
		req.setAttribute("course_db_list", course_db_list);
		return "view/system/paike/addPaike.jsp";
	}

	public String queryEmputyRoom(HttpServletRequest req, HttpServletResponse resp){
	
		String courseIdStr = req.getParameter("courseIdStr");
		String time = req.getParameter("time");
		String date = req.getParameter("date");
		int courseId = Integer.parseInt(courseIdStr);
		
		req.getSession().setAttribute("courseId", courseId);
		req.getSession().setAttribute("time", time);
		req.getSession().setAttribute("date", date);
		List<PaiKe> classRoom_list_em = new ArrayList<PaiKe>();
		
		List<PaiKe> classRoom_all_list = classRoomService.queryAllClassRoom();
		for (PaiKe paiKe_all : classRoom_all_list) {
			//System.out.println(paiKe_all.getRdate());
			if(paiKe_all.getRdate().equals("0"))
			{
				classRoom_list_em.add(paiKe_all);
			}else{
				
				List<PaiKe> roomCourse_db_list = classRoomService.queryAllRoomByRoomId(paiKe_all.getRoomId());
				for (PaiKe paiKe : roomCourse_db_list) {
					if(!paiKe.getTime().equals(time)||!paiKe.getDate().equals(date)){
						classRoom_list_em.add(paiKe);
					}
			}
		}
		}
		req.setAttribute("classRoom_list_em", classRoom_list_em);
		return "view/system/paike/addPaike2.jsp";
		/*req.getSession().setAttribute("courseId", courseId);
		req.getSession().setAttribute("time", time);
		req.getSession().setAttribute("date", date);
		//System.out.println("time"+time+"-----date"+date);
		List<PaiKe> classRoom_list_em = new ArrayList<PaiKe>();
		List<PaiKe> classRoom_all_list = classRoomService.queryAllRoom();
		for (PaiKe classRoom : classRoom_all_list) {
			if (!classRoom.getState().equals(time) || !classRoom.getDate().equals(date))
				{//System.out.println("state:"+classRoom.getState()+"----date:"+classRoom.getDate());
				classRoom_list_em.add(classRoom);}
		}
		req.setAttribute("classRoom_list_em", classRoom_list_em);
		return "view/system/paike/addPaike2.jsp";*/
		
	}

	public String add(HttpServletRequest req, HttpServletResponse resp) {

		int courseId = (int) req.getSession().getAttribute("courseId");
		String time = (String) req.getSession().getAttribute("time");
		String date = (String) req.getSession().getAttribute("date");
		String roomIdStr = req.getParameter("roomIdStr");
		int roomId = Integer.parseInt(roomIdStr);

		RoomCourse roomCourse = new RoomCourse();
		roomCourse.setCourseId(courseId);
		roomCourse.setRoomId(roomId);
		roomCourse.setTime(time);
		roomCourse.setDate(date);

		int n = roomCourseService.addRoomCourseByRoomCourse(roomCourse);

		List<PaiKe> paikeList_db_list = courseService.queryPkBycourseId();

		req.setAttribute("paikeList_db_list", paikeList_db_list);
		// 2、跳转页面
		return "view/system/paike/pakeList.jsp";
	}

	public String delete(HttpServletRequest req, HttpServletResponse resp) {
		String courseIdStr = req.getParameter("courseIdStr");
		String roomCourseIdStr = req.getParameter("roomCourseIdStr");
		String roomIdStr = req.getParameter("roomIdStr");

		int courseId = Integer.parseInt(courseIdStr);
		int roomCourseId = Integer.parseInt(roomCourseIdStr);
		int roomId = Integer.parseInt(roomIdStr);

		int n = courseService.updateCourseStateByCourseId(courseId);
		int m = classRoomService.updateRoomStateByRoomId(roomId);
		int i = roomCourseService.deleteRoomCourseByRoomCourseId(roomCourseId);

		List<PaiKe> paikeList_db_list = courseService.queryPkBycourseId();

		req.setAttribute("paikeList_db_list", paikeList_db_list);
		// 2、跳转页面
		return "view/system/paike/pakeList.jsp";
	}

	public String updateUI(HttpServletRequest req, HttpServletResponse resp) {
		String courseIdStr = req.getParameter("courseIdStr");
		int courseId = Integer.parseInt(courseIdStr);

		List<PaiKe> paike_db_list = courseService.queryPkBycourseId(courseId);
		req.setAttribute("paike_db_list", paike_db_list);

		return "view/system/paike/updatePaike.jsp";
	}

	public String queryEmputyRoom1(HttpServletRequest req, HttpServletResponse resp) {

		String courseIdStr = req.getParameter("courseIdStr");
		String time = req.getParameter("time");
		String date = req.getParameter("date");
		String preroomIdStr = req.getParameter("preroomIdStr");
		String roomCourseId = req.getParameter("roomCourseId");
		int courseId = Integer.parseInt(courseIdStr);

		req.getSession().setAttribute("courseId1", courseId);
		req.getSession().setAttribute("time1", time);
		req.getSession().setAttribute("date1", date);
		req.getSession().setAttribute("preroomId1", Integer.parseInt(preroomIdStr));
		// req.getSession().setAttribute("roomCourseId1", roomCourseId);
		System.out.println("time" + time + "-----date" + date);
		List<PaiKe> classRoom_list_em = new ArrayList<PaiKe>();
		
		List<PaiKe> classRoom_all_list = classRoomService.queryAllClassRoom();
		for (PaiKe paiKe_all : classRoom_all_list) {
			
			if(paiKe_all.getRdate().equals("0"))
			{
				System.out.println(paiKe_all.getRdate()+"---name"+paiKe_all.getRoomName());
				classRoom_list_em.add(paiKe_all);
			}else if(!paiKe_all.getRdate().equals("0")){
				
				List<PaiKe> roomCourse_db_list = classRoomService.queryAllRoomByRoomId(paiKe_all.getRoomId());
				for (PaiKe paiKe : roomCourse_db_list) {
					if((!paiKe.getTime().equals(time)||!paiKe.getDate().equals(date))&&paiKe.getCourseId()!=courseId){
						classRoom_list_em.add(paiKe);
					}
			}
		}
		}
		req.setAttribute("classRoom_list_em", classRoom_list_em);
		return "view/system/paike/updatePaike2.jsp";

	}

	public String update(HttpServletRequest req, HttpServletResponse resp) {

		String roomIdStr = req.getParameter("roomIdStr");
		int roomId = Integer.parseInt(roomIdStr);
		int courseId = (int) req.getSession().getAttribute("courseId1");
		String time = (String) req.getSession().getAttribute("time1");
		String date = (String) req.getSession().getAttribute("date1");
		int preroomId = (int) req.getSession().getAttribute("preroomId1");
		// int roomCourseId = (int)
		// req.getSession().getAttribute("roomCourseId1");

		PaiKe paike = new PaiKe();
		paike.setCourseId(courseId);
		paike.setRoomId(roomId);
		// paike.setRoomCourseId(roomCourseId);
		paike.setTime(time);
		paike.setDate(date);
		paike.setPreroomId(preroomId);

		int n = courseService.updateCourseByPaiKe(paike);

		List<PaiKe> paikeList_db_list = courseService.queryPkBycourseId();

		req.setAttribute("paikeList_db_list", paikeList_db_list);
		// 2、跳转页面
		return "view/system/paike/pakeList.jsp";

	}

}
