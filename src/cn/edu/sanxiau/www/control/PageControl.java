package cn.edu.sanxiau.www.control;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.edu.sanxiau.www.model.PaiKe;
import frame.utils.control.BaseControl2;
import frame.utils.model.PageBean;

public class PageControl extends BaseControl2 {
	
	public int getPC(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String value = req.getParameter("pc");
		if (value == null || value.trim().isEmpty()) {
			return 1;
		}
		return Integer.parseInt(value);

	}
	
	public String listStu(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		
		int pc = getPC(req, resp);
		// b、给定ps的值:每页记录数（page size）
		int ps = 5;

		// 3、调用业务
		PageBean<PaiKe> pageBean = pageService.queryAllPage(pc, ps);
		req.setAttribute("pb", pageBean);
		
		return "view/system/paike/pakeListStu.jsp";
	}

}
