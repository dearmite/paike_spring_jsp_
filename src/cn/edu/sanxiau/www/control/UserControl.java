package cn.edu.sanxiau.www.control;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.edu.sanxiau.www.model.User;

import frame.utils.control.BaseControl2;

public class UserControl extends BaseControl2 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4902327483202817275L;

	// 1.查询
	public String list(HttpServletRequest req, HttpServletResponse resp) {
		// 1、接受数据

		// 3、调用业务
		List<User> user_db_list = userService.queryAllUser();
//		for (User user : user_db_list) {
//			System.out.println("----------list()-----------" + user);
//		}
		// 返回数据到页面
		req.setAttribute("user_db_list", user_db_list);

		// 2、跳转页面
		return "view/system/user/userList.jsp";

	}

	// 2.增加
	public String add(HttpServletRequest req, HttpServletResponse resp) {
		// 1、接受数据
		String name = req.getParameter("name");
		String sex = req.getParameter("sex");
		String adress = req.getParameter("adress");
		String tel = req.getParameter("tel");
		String userName = req.getParameter("userName");
		String password = req.getParameter("password");

		// 装载
		User user = new User();
		user.setName(name);
		user.setSex(sex);
		user.setAdress(adress);
		user.setTel(tel);
		user.setUserName(userName);
		user.setPassword(password);

//		System.out.println("-----------add()----------" + user);

		// 3、调用业务
		int n = userService.addUserByUser(user);

		// 返回数据到页面
		List<User> user_db_list = userService.queryAllUser();
		req.setAttribute("user_db_list", user_db_list);
		// 2、跳转页面
		return "view/system/user/userList.jsp";

	}

	// 3.删除
	public String delete(HttpServletRequest req, HttpServletResponse resp) {

		// 1、接受数据
		String userIdStr = req.getParameter("userIdStr");
//		System.out.println("-----------delete()----------" + userIdStr);
		// 字符类型转成数字类型
		int userId = Integer.parseInt(userIdStr);

		// 3、调用业务
		int m = userService.deleteUserByUserId(userId);

		// 返回数据到页面
		List<User> user_db_list = userService.queryAllUser();
		req.setAttribute("user_db_list", user_db_list);
		// 2、跳转页面
		return "view/system/user/userList.jsp";

	}

	// 4.修改ui
	public String updateUI(HttpServletRequest req, HttpServletResponse resp) {

		// 1、接受数据
		String userIdStr = req.getParameter("userIdStr");
		// 字符转成数字类型
		int userId = Integer.parseInt(userIdStr);
		// 3、调用业务
		User user_db = userService.queryUserByUserId(userId);
//		System.out.println("-------------update()--------" + user_db);

		// 返回数据到页面
		req.setAttribute("user_db", user_db);

		// 2、跳转页面
		return "view/system/user/updateUser.jsp";
	}

	// 4.修改
	public String update(HttpServletRequest req, HttpServletResponse resp) {

		// 1、接受数据
		String userIdStr = req.getParameter("userIdStr");
		int userId = Integer.parseInt(userIdStr);
		String name = req.getParameter("name");
		String sex = req.getParameter("sex");
		String adress = req.getParameter("adress");
		String tel = req.getParameter("tel");
		String userName = req.getParameter("userName");
		String password = req.getParameter("password");

		// 实例化
		User user = new User();
		user.setUserId(userId);
		user.setName(name);
		user.setSex(sex);
		user.setAdress(adress);
		user.setTel(tel);
		user.setUserName(userName);
		user.setPassword(password);

//		System.out.println("-------------update()--------" + user);
		// 3、调用业务
		int n = userService.updateUserByUser(user);

		// 2、跳转页面
		// 返回数据到页面
		List<User> user_db_list = userService.queryAllUser();
		req.setAttribute("user_db_list", user_db_list);
		// 2、跳转页面
		return "view/system/user/userList.jsp";

	}

	// 5.查询
	public String queryUserByUserId(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("----------queryUserByUserId()-----------");
		// 1、接受数据

		// 2、跳转页面
		// 3、调用业务
		return null;

	}

	// 6.查询All
	public String queryAllUser(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("----------queryAllUser()-----------");
		// 1、接受数据

		// 2、跳转页面
		// 3、调用业务
		return null;

	}

	// 6.查询All
	public String queryAllUserwwww(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("----------queryAllUserwwww()-----------");
		// 1、接受数据
		// 2、跳转页面
		// 3、调用业务
		return null;

	}

	// 6.查询All
	public String queryAllUserwwww2(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("----------queryAllUserwwww2()--------queryAllUserwwww2---");
		// 1、接受数据
		// 2、跳转页面
		// 3、调用业务
		return null;

	}

}
