package cn.edu.sanxiau.www.control;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.edu.sanxiau.www.model.ClassRoom;
import cn.edu.sanxiau.www.model.Role;
import cn.edu.sanxiau.www.model.User;
import cn.edu.sanxiau.www.model.User2;
import frame.utils.control.BaseControl2;
import frame.utils.model.PageBean;

public class ClassRoomControl extends BaseControl2 {
	
	public String list(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// 1、接受数据
		// a、 获取页面传递的pc:当前页码（page code）
		int pc = getPC(req, resp);
		// b、给定ps的值:每页记录数（page size）
		int ps = 5;


		// 3、调用业务
		PageBean<ClassRoom> pageBean = classRoomService.queryAllPage(pc, ps);
		req.setAttribute("pb", pageBean);
		List<ClassRoom> classroom_db_list = classRoomService.queryAllClassroom();
		// for (Role role : role_db_list) {
		// System.out.println("----------list()-----------" + role);
		// }
		// 返回数据到页面
		req.setAttribute("classroom_db_list", classroom_db_list);

		// 2、跳转页面
		return "view/system/classroom/classroomList.jsp";

	}
	
	// 2.增加
	public String add(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		
		// 1、接受数据
		// a、 获取页面传递的pc:当前页码（page code）
		int pc = getPC(req, resp);
		// b、给定ps的值:每页记录数（page size）
		int ps = 5;


		// 3、调用业务
		PageBean<ClassRoom> pageBean = classRoomService.queryAllPage(pc, ps);
		req.setAttribute("pb", pageBean);
		
		// 1、接受数据
		String roomName = req.getParameter("roomName");
		String capacity = req.getParameter("capacity");
		String state = req.getParameter("state");

		// 装载
		ClassRoom classroom = new ClassRoom();
		classroom.setRoomName(roomName);
		classroom.setCapacity(capacity);
		classroom.setState(state);

//		System.out.println("-----------add()----------" + user);

		// 3、调用业务
		int n = classRoomService.addClassRoomByClassRoomr(classroom);

		// 返回数据到页面
		List<ClassRoom> classroom_db_list = classRoomService.queryAllClassroom();
		req.setAttribute("classroom_db_list", classroom_db_list);
		// 2、跳转页面
		resp.sendRedirect("http://localhost:8080/XGYQSystem13/classRoomControl.do?method=list");
		return "";

	}
	
	// 3.删除
		public String delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{

			// 1、接受数据
			// a、 获取页面传递的pc:当前页码（page code）
			int pc = getPC(req, resp);
			// b、给定ps的值:每页记录数（page size）
			int ps = 5;


			// 3、调用业务
			PageBean<ClassRoom> pageBean = classRoomService.queryAllPage(pc, ps);
			req.setAttribute("pb", pageBean);
			
			
			// 1、接受数据
			String roomIdStr = req.getParameter("roomIdStr");
//			System.out.println("-----------delete()----------" + userIdStr);
			// 字符类型转成数字类型
			int roomId = Integer.parseInt(roomIdStr);

			// 3、调用业务
			int m = classRoomService.deleteClassRoomByClassRoomId(roomId);

			// 返回数据到页面
			List<ClassRoom> classroom_db_list = classRoomService.queryAllClassroom();
			req.setAttribute("classroom_db_list", classroom_db_list);
			// 2、跳转页面
			resp.sendRedirect("http://localhost:8080/XGYQSystem13/classRoomControl.do?method=list");
			return "";

		}
		
		// 4.修改ui
		public String updateUI(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{

			// 1、接受数据
			// a、 获取页面传递的pc:当前页码（page code）
			int pc = getPC(req, resp);
			// b、给定ps的值:每页记录数（page size）
			int ps = 5;


			// 3、调用业务
			PageBean<ClassRoom> pageBean = classRoomService.queryAllPage(pc, ps);
			req.setAttribute("pb", pageBean);
			
			
			// 1、接受数据
			String roomIdStr = req.getParameter("roomIdStr");
			// 字符转成数字类型
			int roomId = Integer.parseInt(roomIdStr);
			// 3、调用业务
			ClassRoom classroom_db = classRoomService.queryClassRoomByClassRoomId(roomId);
//			System.out.println("-------------update()--------" + classroom_db);

			// 返回数据到页面
			req.setAttribute("classroom_db", classroom_db);

			// 2、跳转页面
			return "view/system/classroom/updateClassRoom.jsp";
		}
		
		// 4.修改
		public String update(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{

			// 1、接受数据
			// a、 获取页面传递的pc:当前页码（page code）
			int pc = getPC(req, resp);
			// b、给定ps的值:每页记录数（page size）
			int ps = 5;


			// 3、调用业务
			PageBean<ClassRoom> pageBean = classRoomService.queryAllPage(pc, ps);
			req.setAttribute("pb", pageBean);
			
			// 1、接受数据
			String roomIdStr = req.getParameter("roomIdStr");
			int roomId = Integer.parseInt(roomIdStr);
			String roomName = req.getParameter("roomName");
			String capacity = req.getParameter("capacity");
			String state = req.getParameter("state");

			// 实例化
			ClassRoom classroom = new ClassRoom();
			classroom.setRoomId(roomId);
			classroom.setRoomName(roomName);
			classroom.setCapacity(capacity);
			classroom.setState(state);

			 System.out.println("-------------update()--------" + classroom);
			// 3、调用业务
			int n = classRoomService.updateClassRoomByClassRoom(classroom);

			// 返回数据到页面
			List<ClassRoom> classroom_db_list = classRoomService.queryAllClassroom();
			req.setAttribute("classroom_db_list", classroom_db_list);
			// 2、跳转页面
			resp.sendRedirect("http://localhost:8080/XGYQSystem13/classRoomControl.do?method=list");
			return "";

		}
		
		public int getPC(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			String value = req.getParameter("pc");
			if (value == null || value.trim().isEmpty()) {
				return 1;
			}
			return Integer.parseInt(value);

		}

}
