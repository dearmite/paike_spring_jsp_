package cn.edu.sanxiau.www.control;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.edu.sanxiau.www.model.Permission;
import cn.edu.sanxiau.www.model.PolePermission;
import cn.edu.sanxiau.www.model.Role;
import frame.utils.control.BaseControl2;

public class PermissionControl extends BaseControl2 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 184346669082886740L;

	// 1.查询
	public String list(HttpServletRequest req, HttpServletResponse resp) {
		// 1、接受数据

		// 3、调用业务
		List<Permission> permission_db_list = permissionService.queryAllPermission();

		// 返回数据到页面
		req.setAttribute("permission_db_list", permission_db_list);
		// 2、跳转页面
		return "view/system/permission/permissionList.jsp";

	}

	// 2.增加UI
	public String addUI(HttpServletRequest req, HttpServletResponse resp) {
		// 1、接受数据

		// 3、调用业务
		List<Permission> permission_db_list = permissionService.queryAllPermission();
		req.setAttribute("permission_db_list", permission_db_list);

		// 2、跳转页面
		return "view/system/permission/addPermission.jsp";

	}

	// 2.增加
	public String add(HttpServletRequest req, HttpServletResponse resp) {
		// 1、接受数据
		String pIdStr = req.getParameter("pIdStr");
		// 字符类型转成int
		int pId = Integer.parseInt(pIdStr);
		String name = req.getParameter("name");
		String type = req.getParameter("type");
		String url = req.getParameter("url");
		String state = req.getParameter("state");

		// 封装
		Permission permission = new Permission();
		permission.setpId(pId);
		permission.setName(name);
		permission.setType(type);
		permission.setUrl(url);
		permission.setState(state);

		// System.out.println("--------------------" + permission);

		// 3、调用业务
		int n = permissionService.addPermissionByPermission(permission);

		// 返回数据到页面
		List<Permission> permission_db_list = permissionService.queryAllPermission();
		req.setAttribute("permission_db_list", permission_db_list);
		// 2、跳转页面
		return "view/system/permission/permissionList.jsp";

	}

	// 3.删除
	public String delete(HttpServletRequest req, HttpServletResponse resp) {

		// 1、接受数据
		String permissionIdStr = req.getParameter("permissionIdStr");
		// 字符类型转成数字类型
		int permissionId = Integer.parseInt(permissionIdStr);
		// System.out.println("-----------delete()----------" + permissionId);

		// 3、调用业务
		int m = permissionService.deletePermissionByPermissionId(permissionId);

		// 返回数据到页面
		List<Permission> permission_db_list = permissionService.queryAllPermission();
		req.setAttribute("permission_db_list", permission_db_list);
		// 2、跳转页面
		return "view/system/permission/permissionList.jsp";

	}

	// 4.修改ui
	public String updateUI(HttpServletRequest req, HttpServletResponse resp) {

		// 1、接受数据
		String permissionIdStr = req.getParameter("permissionIdStr");
		// 字符转成数字类型
		int permissionId = Integer.parseInt(permissionIdStr);
		System.out.println(permissionId);
		// 3、调用业务
		// a.修改数据
		Permission permission_db = permissionService.queryPermissionByPermissionId(permissionId);
		req.setAttribute("permission_db", permission_db);
		// System.out.println(permission_db);

		// b.父级节点数据
		List<Permission> permission_db_list = permissionService.queryAllPermission();
		req.setAttribute("permission_db_list", permission_db_list);

		// c.父级节点选中数据
		int pId = permission_db.getpId();
		Permission permission_db_xz = permissionService.queryPermissionByPermissionId(pId);
		req.setAttribute("permission_db_xz", permission_db_xz);
		System.err.println(permission_db_xz);
		// 2、跳转页面
		return "view/system/permission/updatePermission.jsp";
	}

	// 4.修改
	public String update(HttpServletRequest req, HttpServletResponse resp) {

		// 1、接受数据
		String pIdStr = req.getParameter("pIdStr");
		String permissionIdStr = req.getParameter("permissionIdStr");
		// 字符转成数字类型
		int pId = Integer.parseInt(pIdStr);
		int permissionId = Integer.parseInt(permissionIdStr);

		String name = req.getParameter("name");
		String type = req.getParameter("type");
		String url = req.getParameter("url");
		String state = req.getParameter("state");

		// 封装
		Permission permission = new Permission();
		permission.setPermissionId(permissionId);
		permission.setpId(pId);
		permission.setName(name);
		permission.setType(type);
		permission.setUrl(url);
		permission.setState(state);
		System.err.println(permission);
		// 3、调用业务
		int n = permissionService.updatePermissionByPermission(permission);
		// 2、跳转页面
		// 返回数据到页面
		List<Permission> permission_db_list = permissionService.queryAllPermission();
		req.setAttribute("permission_db_list", permission_db_list);

		return "view/system/permission/permissionList.jsp";
	}

	// 5.分配菜单UI
	public String FPRoleUI(HttpServletRequest req, HttpServletResponse resp) {
		// 1、接受数据
		String roleIdStr = req.getParameter("roleIdStr");
		int roleId = Integer.parseInt(roleIdStr);
		System.err.println(roleId);
		// 3、调用业务
		// 准备数据
		// a.角色数据
		Role role_db = roleService.queryRoleByRoleId(roleId);
		req.setAttribute("role_db", role_db);
		// b.菜单数据集合
		List<Permission> permission_db_list = permissionService.queryAllPermission();
		req.setAttribute("permission_db_list", permission_db_list);
		for (Permission permission : permission_db_list) {
			System.err.println(permission);
		}

		// d.选择菜单数据
		// (XZ:选中)
		List<Permission> permission_dbListXZ = permissionService.queryPermissionByroleId(roleId);
		List<Integer> list = new ArrayList<Integer>();
		for (Permission permission : permission_dbListXZ) {
			list.add(permission.getPermissionId());
		}
		req.setAttribute("ids", list);

		// 2、跳转页面

		return "view/system/permission/FPPermissionUI.jsp";
	}

	// 5.分配菜单
	public String FPRole(HttpServletRequest req, HttpServletResponse resp) {
		// 1、接受数据
		String roleIdStr = req.getParameter("roleIdStr");
		int roleId = Integer.parseInt(roleIdStr);
		System.err.println(roleId);

		// -->先删除，以roleId删除
		int m = polePermissionService.deletePolePermissionByRoleId(roleId);

		// 字符数组
		String[] permissionIdStrS = req.getParameterValues("permissionIdStrS");
		for (String permissionIdStr : permissionIdStrS) {
			int permissionId = Integer.parseInt(permissionIdStr);
			System.out.println(permissionId);
			// 封装
			PolePermission polePermission = new PolePermission();
			polePermission.setRoleId(roleId);
			polePermission.setPermissionId(permissionId);

			// 3、调用业务
			int n = polePermissionService.addPolePermissionByPolePermission(polePermission);
		}

		// 2、跳转页面
		// 准备数据
		// a.角色数据
		Role role_db = roleService.queryRoleByRoleId(roleId);
		req.setAttribute("role_db", role_db);
		// b.菜单数据集合
		List<Permission> permission_db_list = permissionService.queryAllPermission();
		req.setAttribute("permission_db_list", permission_db_list);

		// c.提示
		req.setAttribute("message", "权限分配成功！重新分配吗？");

		// d.选中菜单数据
		// List<Permission> permission_db_list_XZ =
		// permissionService.queryPermissionByroleId(roleId);
		// for (Permission permission : permission_db_list_XZ) {
		// System.out.println(permission);
		// }
		// req.setAttribute("permission_db_list_XZ", permission_db_list_XZ);

		// d.选择菜单数据
		// (XZ:选中)
		List<Permission> permission_dbListXZ = permissionService.queryPermissionByroleId(roleId);
		List<Integer> list = new ArrayList<Integer>();
		for (Permission permission : permission_dbListXZ) {
			list.add(permission.getPermissionId());
		}
		req.setAttribute("ids", list);

		return "view/system/permission/FPPermissionUI.jsp";
	}
}
