package cn.edu.sanxiau.www.service.impl;

import java.util.List;

import cn.edu.sanxiau.www.dao.User2Dao;
import cn.edu.sanxiau.www.dao.impl.User2DaoImpl;
import cn.edu.sanxiau.www.model.User2;
import cn.edu.sanxiau.www.service.User2Service;
import frame.utils.model.PageBean;

public class User2ServiceImpl implements User2Service {
	User2Dao user2Dao = new User2DaoImpl();

	@Override
	public PageBean<User2> queryAllPage(int pc, int ps) {
		// 实例化PageBean
		PageBean<User2> pb = new PageBean<User2>();
		pb.setPc(pc);
		pb.setPs(ps);

		// 查询总记录数:总记录数（total record）
		int tr = user2Dao.queryUser2TotalRecord();
		pb.setTr(tr);

		// 查询当前页的记录
		List<User2> beanList = user2Dao.queryCurrentPageDataList((pc - 1) * ps, ps);
		pb.setBeanList(beanList);
		return pb;
	}

}
