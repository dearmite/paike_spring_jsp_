package cn.edu.sanxiau.www.service.impl;

import java.util.List;

import cn.edu.sanxiau.www.dao.UserDao;
import cn.edu.sanxiau.www.dao.impl.UserDaoImpl;
import cn.edu.sanxiau.www.model.User;

import cn.edu.sanxiau.www.service.UserService;
import frame.utils.model.PageBean;

public class UserServiceImpl implements UserService {
	// 实例化DAO接口
	UserDao userDao = new UserDaoImpl();

	@Override
	public User queryUserByUser(User user) {
		User user_db = userDao.queryUserByUser(user);
		return user_db;
	}

	@Override
	public List<User> queryAllUser() {
		return userDao.queryAllUser();
	}

	@Override
	public int addUserByUser(User user) {
		return userDao.addUserByUser(user);
	}

	@Override
	public int deleteUserByUserId(int userId) {
		return userDao.deleteUserByUserId(userId);
	}

	@Override
	public User queryUserByUserId(int userId) {
		return userDao.queryUserByUserId(userId);
	}

	@Override
	public int updateUserByUser(User user) {
		return userDao.updateUserByUser(user);
	}



}
