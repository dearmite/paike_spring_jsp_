package cn.edu.sanxiau.www.service.impl;

import cn.edu.sanxiau.www.dao.PolePermissionDao;
import cn.edu.sanxiau.www.dao.impl.PolePermissionDaoImpl;
import cn.edu.sanxiau.www.model.PolePermission;
import cn.edu.sanxiau.www.service.PolePermissionService;

public class PolePermissionServiceImpl implements PolePermissionService {
	PolePermissionDao polePermissionDao = new PolePermissionDaoImpl();

	@Override
	public int addPolePermissionByPolePermission(PolePermission polePermission) {
		return polePermissionDao.addPolePermissionByPolePermission(polePermission);
	}

	@Override
	public int deletePolePermissionByRoleId(int roleId) {
		return polePermissionDao.deletePolePermissionByRoleId(roleId);
	}

}
