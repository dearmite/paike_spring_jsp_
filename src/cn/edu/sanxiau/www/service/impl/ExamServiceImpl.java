package cn.edu.sanxiau.www.service.impl;

import java.util.List;

import cn.edu.sanxiau.www.dao.ExamDao;
import cn.edu.sanxiau.www.dao.impl.ExamDaoImpl;
import cn.edu.sanxiau.www.model.Course;
import cn.edu.sanxiau.www.model.Exam;
import cn.edu.sanxiau.www.model.PaiRoom;
import cn.edu.sanxiau.www.service.ExamService;
import frame.utils.model.PageBean;

public class ExamServiceImpl implements ExamService {
	ExamDao examDao = new ExamDaoImpl();

	public List<Exam> queryAllExam() {
		return examDao.queryAllExam();
	}

	public int addExamByExam(Exam exam) {
		return examDao.addExamByExam(exam);
	}

	public int deleteExamByExamId(int examId) {
		return examDao.deleteExamByExamId(examId);
	}

	public Exam queryExamByExamId(int examId) {
		return examDao.queryExamByExamId(examId);
	}

	public int updateExamByExam(Exam exam) {
		return examDao.updateExamByExam(exam);
	}

	public PageBean<Exam> queryAllPage(int pc, int ps) {
		PageBean<Exam> pb = new PageBean<Exam>();
		pb.setPc(pc);
		pb.setPs(ps);

		// 查询总记录数:总记录数（total record）
		int tr = examDao.queryUser2TotalRecord();
		pb.setTr(tr);

		// 查询当前页的记录
		List<Exam> beanList = examDao.queryCurrentPageDataList((pc - 1) * ps, ps);
		pb.setBeanList(beanList);
		return pb;
	}

	
	
	@Override
	public List<PaiRoom> queryPrByExamId() {   //list
		return examDao.queryPrByExamId();
	}

	@Override
	public List<Exam> queryAllExamByState(String string) {         ////addUI2
		return examDao.queryAllExamByState(string);
	}

}
