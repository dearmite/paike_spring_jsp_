package cn.edu.sanxiau.www.service.impl;

import java.util.List;

import cn.edu.sanxiau.www.dao.RoleDao;
import cn.edu.sanxiau.www.dao.impl.RoleDaoImpl;
import cn.edu.sanxiau.www.model.Role;
import cn.edu.sanxiau.www.service.RoleService;

public class RoleServiceImpl implements RoleService {
	// 注入DAO接口
	RoleDao roleDao = new RoleDaoImpl();

	@Override
	public List<Role> queryAllRole() {
		return roleDao.queryAllRole();
	}

	@Override
	public int addRoleByRole(Role role) {
		return roleDao.addRoleByRole(role);
	}

	@Override
	public int deleteRoleByRoleId(int roleId) {
		return roleDao.deleteRoleByRoleId(roleId);
	}

	@Override
	public Role queryRoleByRoleId(int roleId) {
		return roleDao.queryRoleByRoleId(roleId);
	}

	@Override
	public int updateRoleByRoler(Role role) {
		return roleDao.updateRoleByRoler(role);
	}

	@Override
	public Role queryRoleByUserId(int userId) {
		return roleDao.queryRoleByUserId(userId);
	}

}
