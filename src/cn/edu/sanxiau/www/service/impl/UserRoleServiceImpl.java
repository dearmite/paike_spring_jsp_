package cn.edu.sanxiau.www.service.impl;

import cn.edu.sanxiau.www.dao.UserRoleDao;
import cn.edu.sanxiau.www.dao.impl.UserRoleDaoImpl;
import cn.edu.sanxiau.www.model.UserRole;
import cn.edu.sanxiau.www.service.UserRoleService;

public class UserRoleServiceImpl implements UserRoleService {
	UserRoleDao userRoleDao = new UserRoleDaoImpl();

	@Override
	public int addUserRoleByUserRole(UserRole userRole) {
		return userRoleDao.addUserRoleByUserRole(userRole);
	}

	@Override
	public int deleteUserRoleByUserId(int userId) {
		return userRoleDao.deleteUserRoleByUserId(userId);
	}

}
