package cn.edu.sanxiau.www.service;

import java.util.List;

import cn.edu.sanxiau.www.model.Exam;
import cn.edu.sanxiau.www.model.PaiRoom;
import frame.utils.model.PageBean;

public interface ExamService {

	List<Exam> queryAllExam();

	int addExamByExam(Exam exam);

	int deleteExamByExamId(int examId);

	Exam queryExamByExamId(int examId);

	int updateExamByExam(Exam exam);

	PageBean<Exam> queryAllPage(int pc, int ps);
	
	

	List<PaiRoom> queryPrByExamId();  //list
	
	List<Exam> queryAllExamByState(String string);       //addUI2

	 

}
