package cn.edu.sanxiau.www.service;

import java.util.List;

import cn.edu.sanxiau.www.model.Course;
import cn.edu.sanxiau.www.model.PaiKe;
import frame.utils.model.PageBean;

public interface CourseService {

	List<Course> queryAllCourse();

	int addCourseByCourse(Course course);

	int deleteCourseByCourseId(int courseId);

	Course queryCourseByCourseId(int courseId);

	int updateCourseByCourse(Course course);

	PageBean<Course> queryAllPage(int pc, int ps);
	List<Course> queryAllCourseByState(String state);

	List<PaiKe> queryPkBycourseId();

	int updateCourseStateByCourseId(int courseId);

	List<PaiKe> queryPkBycourseId(int courseId);

	int updateCourseByPaiKe(PaiKe paike);

}
