package cn.edu.sanxiau.www.service;

import cn.edu.sanxiau.www.model.UserRole;

public interface UserRoleService {

	int addUserRoleByUserRole(UserRole userRole);

	int deleteUserRoleByUserId(int userId);

}
