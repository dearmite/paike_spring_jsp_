package cn.edu.sanxiau.www.service;

import cn.edu.sanxiau.www.model.User2;
import frame.utils.model.PageBean;

public interface User2Service {

	PageBean<User2> queryAllPage(int pc, int ps);

}
