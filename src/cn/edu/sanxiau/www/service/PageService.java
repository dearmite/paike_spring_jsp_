package cn.edu.sanxiau.www.service;

import cn.edu.sanxiau.www.model.PaiKe;
import frame.utils.model.PageBean;

public interface PageService {

	PageBean<PaiKe> queryAllPage(int pc, int ps);

}
