package cn.edu.sanxiau.www.service;

import java.util.List;

import cn.edu.sanxiau.www.model.RoomCourse;

public interface RoomCourseService {

	List<RoomCourse> queryTimeByCourse();

	int addRoomCourseByRoomCourse(RoomCourse roomCourse);

	int deleteRoomCourseByRoomCourseId(int roomCourseId);

}
