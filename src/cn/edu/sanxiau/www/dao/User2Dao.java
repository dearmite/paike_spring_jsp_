package cn.edu.sanxiau.www.dao;

import java.util.List;

import cn.edu.sanxiau.www.model.User2;

public interface User2Dao {

	int queryUser2TotalRecord();

	List<User2> queryCurrentPageDataList(int i, int ps);

}
