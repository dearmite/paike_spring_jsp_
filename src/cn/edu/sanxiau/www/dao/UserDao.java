package cn.edu.sanxiau.www.dao;

import java.util.List;

import cn.edu.sanxiau.www.model.User;

public interface UserDao {

	User queryUserByUser(User user);

	List<User> queryAllUser();

	int addUserByUser(User user);

	int deleteUserByUserId(int userId);

	User queryUserByUserId(int userId);

	int updateUserByUser(User user);


}
