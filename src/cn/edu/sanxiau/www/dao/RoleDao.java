package cn.edu.sanxiau.www.dao;

import java.util.List;

import cn.edu.sanxiau.www.model.Role;

public interface RoleDao {

	List<Role> queryAllRole();

	int addRoleByRole(Role role);

	int deleteRoleByRoleId(int roleId);

	Role queryRoleByRoleId(int roleId);

	int updateRoleByRoler(Role role);

	Role queryRoleByUserId(int userId);

}
