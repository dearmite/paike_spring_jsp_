package cn.edu.sanxiau.www.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import cn.edu.sanxiau.www.dao.UserRoleDao;
import cn.edu.sanxiau.www.model.UserRole;
import frame.utils.dbutil.JdbcUtils;

public class UserRoleDaoImpl implements UserRoleDao {

	@Override
	public int addUserRoleByUserRole(UserRole userRole) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = JdbcUtils.getConnection();
			String sql = "INSERT INTO  sys_user_role (userRoleId,userId,roleId) VALUES(?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, userRole.getUserRoleId());
			pstmt.setInt(2, userRole.getUserId());
			pstmt.setInt(3, userRole.getRoleId());

			int m = pstmt.executeUpdate();
			return m;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}

	@Override
	public int deleteUserRoleByUserId(int userId) {
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = JdbcUtils.getConnection();

			String sql = "DELETE FROM sys_user_role WHERE userId=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, userId);

			int m = pstmt.executeUpdate();
			return m;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

}
