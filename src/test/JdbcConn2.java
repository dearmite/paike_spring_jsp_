package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcConn2 {
	public static void main(String[] args) {
		// 1、定义连接Connection对象
		Connection conn = null;
		// 2、定义预处理对象Statement--》PreparedStatement
		PreparedStatement pstm = null;
		// 3、定义结果集
		ResultSet rs = null;
		// 4、定义mysql驱动
		String driverClassName = "com.mysql.jdbc.Driver";
		// 5、定义数据库连接url
		String url = "jdbc:mysql://localhost:3306/xgyqsystem?characterEncoding=utf-8";
		// 6、定义数据库名
		String userName = "root";
		// 7、定义数据库密码
		String password = "root";
		try {
			// 8、加载驱动
			Class.forName(driverClassName);
			// 9、获取连接
			conn = DriverManager.getConnection(url, userName, password);
			// System.out.println("------------" + conn);
			// 10、定义sql
			String sql = "SELECT * FROM sys_user WHERE userName=?";
			// 11、开启事务
			conn.setAutoCommit(false);
			// 12、创建sql预处理对象
			pstm = conn.prepareStatement(sql);
			// 13、设置参数
			pstm.setString(1, "xm");
			// 14、预处理对象操作sql查询数据库,返回结果集对象集合。
			rs = pstm.executeQuery();
			// 15、遍历结果集对象集合
			while (rs.next()) {
				System.err.println(rs.getString("userId") + " " + rs.getString("name"));
			}
			// 16、 提交事务
			conn.commit();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				// 17、 回滚事务
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				// 18、关闭资源
				if (rs != null) {
					rs.close();
				}
				if (pstm != null) {
					pstm.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}
}
