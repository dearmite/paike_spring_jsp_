//package test;
//
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//import frame.utils.dbutil.JdbcUtils;
//
//public class JdbcConn3 {
//	public static void main(String[] args) {
//	
//		try {
//		
//			// 9、获取连接
//			conn = JdbcUtils.getConnection();
//			// System.out.println("------------" + conn);
//			// 10、定义sql
//			String sql = "SELECT * FROM sys_user WHERE userName=?";
//			// 11、开启事务
//			conn.setAutoCommit(false);
//			// 12、创建sql预处理对象
//			pstm = conn.prepareStatement(sql);
//			// 13、设置参数
//			pstm.setString(1, "xm");
//			// 14、预处理对象操作sql查询数据库,返回结果集对象集合。
//			rs = pstm.executeQuery();
//			// 15、遍历结果集对象集合
//			while (rs.next()) {
//				System.err.println(rs.getString("userId") + " " + rs.getString("name"));
//			}
//			// 16、 提交事务
//			conn.commit();
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		} catch (SQLException e) {
//			e.printStackTrace();
//			try {
//				// 17、 回滚事务
//				conn.rollback();
//			} catch (SQLException e1) {
//				e1.printStackTrace();
//			}
//		} finally {
//			try {
//				// 18、关闭资源
//				if (rs != null) {
//					rs.close();
//				}
//				if (pstm != null) {
//					pstm.close();
//				}
//				if (conn != null) {
//					conn.close();
//				}
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//		}
//
//	}
//}
