package frame.utils.control;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.edu.sanxiau.www.service.UserService;
import cn.edu.sanxiau.www.service.impl.UserServiceImpl;

public class BaseControl extends HttpServlet {
	// 实例化业务接口
	UserService userService = new UserServiceImpl();

	// 分发servlet
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// 页面传递参数
		String method = req.getParameter("method");
		// 判断
		if (method.equals("add")) {
			this.add(req, resp);
		}
		if (method.equals("delete")) {
			this.delete(req, resp);
		}
		if (method.equals("update")) {
			this.update(req, resp);
		}
		if (method.equals("queryUserByUserId")) {
			this.queryUserByUserId(req, resp);
		}

		if (method.equals("list")) {
			this.list(req, resp);
		}
		if (method.equals("queryAllUser")) {
			this.queryAllUser(req, resp);
		}

	}

	// 1.查询
	public String list(HttpServletRequest req, HttpServletResponse resp) {
		// System.out.println("----------list()-----------");
		// 1、接受数据
		// 2、跳转页面
		// 3、调用业务
		return "view/system/user/userList.jsp";

	}

	// 2.增加
	public String add(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("-----------add()----------");
		// 1、接受数据
		// 2、跳转页面
		// 3、调用业务
		return null;

	}

	// 3.删除
	public String delete(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("-----------delete()----------");
		// 1、接受数据
		// 2、跳转页面
		// 3、调用业务
		return null;

	}

	// 4.修改
	public String update(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("-------------update()--------");
		// 1、接受数据
		// 2、跳转页面
		// 3、调用业务
		return null;

	}

	// 5.查询
	public String queryUserByUserId(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("----------queryUserByUserId()-----------");
		// 1、接受数据
		// 2、跳转页面
		// 3、调用业务
		return null;

	}

	// 6.查询All
	public String queryAllUser(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("----------queryAllUser()-----------");
		// 1、接受数据
		// 2、跳转页面
		// 3、调用业务
		return null;

	}
}
