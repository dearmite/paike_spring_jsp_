# 学生权限管理系统排课模块

#### 介绍

开发运行环境：eclipse、tomcat8、jdk1.8、mysql5.7、jstl-1.2.jar，mysql-connector-java-5.1.17.jar，浏览器。
主要采用serlet+jsp +jquery+easyUI做的，以web.xml配置文件的方式来控制页面跳转

####  主要实现以下功能

系统管理：用户管理、角色管理、权限管理。

课程信息管理：课程信息管理。

教室信息管理：教室信息管理。

考试管理：考试管理、考试信息查询。

学生课表：个人课表查询。

排课管理：排课管理。


#### 关于用户管理中分配角色的设计，建立了user表 role表和user-role中间表这三个表进行角色的分配。核心思路，通过SQL内连接的方式，将role 和user-role连接起来，用userID来查用户的角色，进而实现对角色的分配。当然每一次对角色的分配都需要删除原来的角色用户的对应关系。

#####用户管理和角色管理类似的思想完成，不过加了ArrayList集合对权限菜单的管理遍历

#####权限管理就是对权限数据表中的数据渲染到前端进行展示，并有修改和删除功能

#######通过写pagebean<?>bean来来实现的分页功能，推出登录是直接跳转到登录页面

#####首页用了一点json的方式渲染数据



#### 注意事项

 #MySQL
driverClassName = com.mysql.jdbc.Driver
url = jdbc:mysql://localhost:3306/xgyqsystem?characterEncoding=utf-8
userName = root
password = 
注意修改为自己的数据库用户名，密码
 
注意MySQL的MySQL驱动的版本问题。

#### 类图

 ![输入图片说明](https://images.gitee.com/uploads/images/2020/1003/140558_cfdbdadc_7666809.png "屏幕截图.png")


项目运行截图：

![输入图片说明](https://images.gitee.com/uploads/images/2020/1005/205213_edbc7431_7666809.png "XDR)20H8XDAX5Q6(99WIW71.png") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1005/205236_832b3d0e_7666809.png "WY]52I$4A@RZ[36{TZ)EKUN.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1005/205251_b7ce8ca5_7666809.png "SNLY}@QE{LG6XM{_[0~Z3$M.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1005/205312_15e71397_7666809.png "QDM7PT7Y]7IX39XIMHM$2QU.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1005/205327_cc8d9795_7666809.png "Q$MPWOU3%J1)LSQ00[LAK4Q.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1005/205342_6f1e52e6_7666809.png "(EI23JUYLC(HC`JY%_(@(}8.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1005/205355_05a70a22_7666809.png "~C73S45DQ~B2)RXV)09}K97.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1005/205442_77f343a6_7666809.png "71KLDAKM4`EV8$((WU1P[IU.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1005/205500_59710239_7666809.png "71KLDAKM4`EV8$((WU1P[IU.png")


