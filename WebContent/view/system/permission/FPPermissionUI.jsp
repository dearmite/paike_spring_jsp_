<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="<%=basePath%>css/style.css" rel="stylesheet" type="text/css" />
<script src="<%=basePath%>js/jquery.js"></script>
<script src="<%=basePath%>js/pintuer.js"></script>
<script>
	$(function() {
		var array = new Array();
		<c:forEach items="${ids}" var="item" >
		array.push("${item}"); //对象，加引号       
		</c:forEach>
		for (var i = 0; i < array.length; i++) {
			$("#" + array[i]).attr("checked", "checked");
		}
	})
</script>
</head>

<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">首页</a></li>
    <li><a href="#">表单</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    <div class="formtitle"><span>分配菜单：<font color="red">${role_db.name }</font>&nbsp;&nbsp;${message }</span></div>
    <form action="<%=basePath%>permissionControl.do?method=FPRole" method="post">
    <input type="hidden" name="roleIdStr" value="${role_db.roleId }">
    <ul class="forminfo">
    <c:forEach items="${permission_db_list }" var="permission">
       <input type="checkbox" name="permissionIdStrS" value="${permission.permissionId }" id="${permission.permissionId }">${permission.name }<br>
	</c:forEach>
	
	<br>
	&nbsp;
		<br>
	&nbsp;
		<br>
	&nbsp;
		<br>
	&nbsp;
       <li><label>&nbsp;</label><input name="" type="submit" class="btn" value="分配菜单"/></li> 
    </ul>
    </form>
    
    </div>
</body>
</html>
