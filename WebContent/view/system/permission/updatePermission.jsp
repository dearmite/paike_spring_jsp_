<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>

	<div class="place">
		<span>位置：</span>
		<ul class="placeul">
			<li><a href="#">首页</a></li>
			<li><a href="#">表单</a></li>
		</ul>
	</div>

	<div class="formbody">

		<div class="formtitle">
			<span>基本信息</span>
		</div>
		<form action="<%=basePath%>permissionControl.do?method=update"
			method="post">
			<input type="hidden" name="permissionIdStr" value="${permission_db.permissionId }">



			<ul class="forminfo">
				<li><label>父级</label> <select id="pIdStr" name="pIdStr">
						<option class="dfinput">------------------请选择父级节点------------------</option>
						<c:forEach items="${permission_db_list }" var="permission">
						
						<option value="${permission.permissionId }">${permission.name }</option>
						<%-- 	<c:if test="${permission.permissionId == permission_db_xz.pId }"
								var="anth">
								<option value="${permission.permissionId }" selected="selected">${permission.name }</option>
							</c:if>
							<c:if test="${permission.permissionId == permission_db_xz.pId }"
								var="anth">
								<option value="${permission.permissionId }">${permission.name }</option>
							</c:if> --%>
						</c:forEach>
				</select></li>

				<li><label>名称</label><input name="name" type="text"
					class="dfinput" value="${permission_db.name }" /><i>名称不能超过30个字符</i></li>
				<li><label>类型</label><input name="type" type="text"
					class="dfinput" value="${permission_db.type }" /><i>状态不能超过30个字符</i></li>
				<li><label>url</label><input name="url" type="text"
					class="dfinput" value="${permission_db.url }" /><i>名称不能超过30个字符</i></li>
				<li><label>状态</label><input name="state" type="text"
					class="dfinput" value="${permission_db.state }" /><i>状态不能超过30个字符</i></li>
				<li><label>&nbsp;</label><input name="" type="submit"
					class="btn" value="确认保存" /></li>
			</ul>
		</form>

	</div>
</body>
</html>
