<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>考试管理保存界面</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">首页</a></li>
    <li><a href="#">表单</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    <div class="formtitle"><span>基本信息</span></div>
    <form action="<%=basePath%>examControl.do?method=add" method="post">
    <ul class="forminfo">
    <li><label>考试科目</label><input name="examName" type="text" class="dfinput" /><i>考试科目不能超过30个字符</i></li>
    <li><label>考试类型</label><input name="examType" type="text" class="dfinput" /><i>考试类型不能超过30个字符</i></li>
      <li><label>考试状态</label><input name="examState" type="text" class="dfinput" /><i>考试类型不能超过30个字符</i></li>
    <li><label>&nbsp;</label><input name="" type="submit" class="btn" value="确认保存"/></li> 
    </ul>
    </form>
    
    </div>
</body>
</html>