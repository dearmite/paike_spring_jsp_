<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
	<center>
		<h1>修改页面</h1>
		<hr>
		<form action="<%=basePath%>examControl.do?method=update" method="post">
		<input type="hidden" name="examIdStr" value="${exam_db.examId }">
			<table>
				<tr>
					<td>考试科目:</td>
					<td><input type="text" name="examName" value="${exam_db.examName }"></td>
				</tr>
				<tr>
					<td>考试类型:</td>
					<td><input type="text" name="examType" value="${exam_db.examType }"></td>
				</tr>
					<tr>
					<td>考试状态:</td>
					<td><input type="text" name="examState" value="${exam_db.examState }"></td>
				</tr>
				 
				<tr>
					<td><input type="submit" value="修改"></td>
					<td><input type="reset" value="重置"></td>
				</tr>
			</table>
		</form>
	</center>

</body>
</html>