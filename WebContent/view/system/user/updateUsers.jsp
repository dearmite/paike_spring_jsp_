<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
	<center>
		<h1>修改页面</h1>
		<hr>
		<form action="<%=basePath%>userControl.do?method=update" method="post">
		<input type="hidden" name="userIdStr" value="${user_db.userId }">
			<table>
				<tr>
					<td>姓名:</td>
					<td><input type="text" name="name" value="${user_db.name }"></td>
				</tr>
				<tr>
					<td>性别:</td>
					<td><input type="text" name="sex" value="${user_db.sex }"></td>
				</tr>
				<tr>
					<td>地址:</td>
					<td><input type="text" name="adress" value="${user_db.adress }"></td>
				</tr>
				<tr>
					<td>电话:</td>
					<td><input type="text" name="tel" value="${user_db.tel }"></td>
				</tr>
				<tr>
					<td>登录名:</td>
					<td><input type="text" name="userName" value="${user_db.userName }"></td>
				</tr>
				<tr>
					<td>密码:</td>
					<td><input type="text" name="password" value="${user_db.password }"></td>
				</tr>
				<tr>
					<td><input type="submit" value="修改"></td>
					<td><input type="reset" value="重置"></td>
				</tr>
			</table>
		</form>
	</center>

</body>
</html>