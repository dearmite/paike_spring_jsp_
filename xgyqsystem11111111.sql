/*
Navicat MySQL Data Transfer

Source Server         : ff
Source Server Version : 50643
Source Host           : localhost:3306
Source Database       : xgyqsystem

Target Server Type    : MYSQL
Target Server Version : 50643
File Encoding         : 65001

Date: 2020-06-21 00:36:56
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for classroom
-- ----------------------------
DROP TABLE IF EXISTS classroom;
CREATE TABLE classroom (
  roomId int(11) NOT NULL AUTO_INCREMENT,
  roomName varchar(255) DEFAULT NULL,
  capacity varchar(255) DEFAULT NULL,
  state varchar(255) DEFAULT '0',
  date varchar(255) DEFAULT '0',
  PRIMARY KEY (roomId)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of classroom
-- ----------------------------
INSERT INTO classroom VALUES ('1', '明辨3栋403', '45', '2', '2');
INSERT INTO classroom VALUES ('2', '慎思3栋232', '45', '0', '0');
INSERT INTO classroom VALUES ('3', '明辨1栋405', '45', '0', '0');
INSERT INTO classroom VALUES ('4', '慎思3栋324', '45', '0', '0');
INSERT INTO classroom VALUES ('5', '明辨1栋101', '45', '0', '0');
INSERT INTO classroom VALUES ('6', '明辨1栋102', '60', '0', '0');
INSERT INTO classroom VALUES ('7', '明辨1栋103', '60', '0', '0');
INSERT INTO classroom VALUES ('8', '明辨1栋104', '60', '0', '0');
INSERT INTO classroom VALUES ('9', '明辨2栋101', '80', '0', '0');
INSERT INTO classroom VALUES ('10', '明辨2栋102', '80', '0', '0');
INSERT INTO classroom VALUES ('11', '明辨3栋404', '45', '0', '0');

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS course;
CREATE TABLE course (
  courseId int(11) NOT NULL AUTO_INCREMENT,
  courseName varchar(255) DEFAULT NULL,
  courseTeacher varchar(255) DEFAULT NULL,
  score varchar(255) DEFAULT NULL,
  state varchar(255) DEFAULT '0',
  PRIMARY KEY (courseId)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO course VALUES ('1', 'java', '朱老师', '8', '1');
INSERT INTO course VALUES ('2', 'python', '刘老师', '4', '2');
INSERT INTO course VALUES ('3', 'C语言', '廖有成', '5', '0');
INSERT INTO course VALUES ('4', '高数', '刘学飞', '5', '2');
INSERT INTO course VALUES ('5', '线性代数', '刘青华', '4', '0');
INSERT INTO course VALUES ('6', '操作系统', '杨金山', '5', '0');
INSERT INTO course VALUES ('7', '计算机网络', '邱巡', '4', '0');
INSERT INTO course VALUES ('8', '信息技术', '钟志雄', '3', '0');
INSERT INTO course VALUES ('9', '软件工程', '杨鹏飞', '3', '0');
INSERT INTO course VALUES ('10', '概率论', '李老师', '4', '0');
INSERT INTO course VALUES ('11', '软件项目管理', '杨老师', '3', '0');

-- ----------------------------
-- Table structure for exam
-- ----------------------------
DROP TABLE IF EXISTS exam;
CREATE TABLE exam (
  examId int(11) NOT NULL AUTO_INCREMENT,
  examName varchar(255) DEFAULT NULL,
  examType varchar(255) DEFAULT NULL,
  examState varchar(255) DEFAULT NULL,
  PRIMARY KEY (examId)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of exam
-- ----------------------------
INSERT INTO exam VALUES ('1', '大数据', '上机', '0');
INSERT INTO exam VALUES ('6', '云计算', '笔试', '0');
INSERT INTO exam VALUES ('7', '大数据', '上机', '0');
INSERT INTO exam VALUES ('9', '大数据', '上机', '0');
INSERT INTO exam VALUES ('10', '云计算', '笔试', '0');
INSERT INTO exam VALUES ('11', '大数据', '上机', '0');
INSERT INTO exam VALUES ('12', '云计算', '笔试', '0');
INSERT INTO exam VALUES ('13', '大数据', '上机', '0');
INSERT INTO exam VALUES ('14', '云计算', '笔试', '1');

-- ----------------------------
-- Table structure for room_course
-- ----------------------------
DROP TABLE IF EXISTS room_course;
CREATE TABLE room_course (
  room_course_Id int(11) NOT NULL AUTO_INCREMENT,
  courseId int(11) DEFAULT NULL,
  roomId int(11) DEFAULT NULL,
  time varchar(255) DEFAULT NULL,
  date varchar(255) DEFAULT NULL,
  PRIMARY KEY (room_course_Id)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of room_course
-- ----------------------------
INSERT INTO room_course VALUES ('59', '1', '1', '1', '1');
INSERT INTO room_course VALUES ('60', '2', '1', '2', '1');
INSERT INTO room_course VALUES ('61', '4', '1', '2', '2');

-- ----------------------------
-- Table structure for room_exam
-- ----------------------------
DROP TABLE IF EXISTS room_exam;
CREATE TABLE room_exam (
  room_exam_id int(11) NOT NULL AUTO_INCREMENT,
  examId varchar(255) NOT NULL,
  roomld varchar(255) NOT NULL,
  date varchar(255) NOT NULL,
  time varchar(255) NOT NULL,
  PRIMARY KEY (room_exam_id)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of room_exam
-- ----------------------------
INSERT INTO room_exam VALUES ('1', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS sys_permission;
CREATE TABLE sys_permission (
  permissionId int(11) NOT NULL AUTO_INCREMENT COMMENT '权限ID',
  pId int(11) DEFAULT NULL COMMENT '权限ID',
  name varchar(255) DEFAULT NULL COMMENT '名称',
  type varchar(255) DEFAULT NULL COMMENT '类型',
  url varchar(255) DEFAULT NULL COMMENT 'url',
  state varchar(255) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (permissionId)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COMMENT='权限';

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO sys_permission VALUES ('1', '-1', '根节点', '根节点', '无', '1');
INSERT INTO sys_permission VALUES ('2', '1', '系统管理', '菜单', '1', '1');
INSERT INTO sys_permission VALUES ('3', '2', '用户管理', '权限', 'userControl.do?method=list', '1');
INSERT INTO sys_permission VALUES ('4', '2', '角色管理', '权限', 'roleControl.do?method=list', '1');
INSERT INTO sys_permission VALUES ('5', '2', '权限管理', '权限', 'permissionControl.do?method=list', '1');
INSERT INTO sys_permission VALUES ('16', '2', '用户分页', '权限', 'user2Control.do?method=listPage', '1');
INSERT INTO sys_permission VALUES ('17', '1', '课表管理', '菜单', '无', '1');
INSERT INTO sys_permission VALUES ('18', '1', '教室管理', '菜单', '无', '1');
INSERT INTO sys_permission VALUES ('19', '17', '课程信息管理', '权限', 'courseControl.do?method=list', '1');
INSERT INTO sys_permission VALUES ('20', '18', '教室信息管理', '权限', 'classRoomControl.do?method=list', '1');
INSERT INTO sys_permission VALUES ('21', '1', '考试管理', '菜单', '无', '1');
INSERT INTO sys_permission VALUES ('22', '21', '考试信息管理', '权限', 'examControl.do?method=list', '1');
INSERT INTO sys_permission VALUES ('27', '1', '课表管理', '菜单', '无', '1');
INSERT INTO sys_permission VALUES ('28', '27', '课表信息查询', '权限', 'pageControl.do?method=listStu', '1');
INSERT INTO sys_permission VALUES ('29', '1', '排课管理', '菜单', '1', '1');
INSERT INTO sys_permission VALUES ('30', '29', '排课管理', '管理', 'paikeControl.do?method=list', '1');
INSERT INTO sys_permission VALUES ('31', '21', '考试信息查询', '权限', 'pairoomControl.do?method=list ', '1');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS sys_role;
CREATE TABLE sys_role (
  roleId int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  name varchar(255) DEFAULT NULL COMMENT '名称',
  state varchar(255) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (roleId)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO sys_role VALUES ('1', '系统管理员', '1');
INSERT INTO sys_role VALUES ('2', '课表管理员', '1');
INSERT INTO sys_role VALUES ('3', '教室管理员', '1');
INSERT INTO sys_role VALUES ('4', '学生', '1');

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS sys_role_permission;
CREATE TABLE sys_role_permission (
  rolePermissionId int(11) NOT NULL AUTO_INCREMENT,
  roleId int(11) DEFAULT NULL COMMENT '角色ID',
  permissionId int(11) DEFAULT NULL COMMENT '权限ID',
  PRIMARY KEY (rolePermissionId)
) ENGINE=InnoDB AUTO_INCREMENT=548 DEFAULT CHARSET=utf8 COMMENT='角色权限';

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO sys_role_permission VALUES ('287', '3', '1');
INSERT INTO sys_role_permission VALUES ('288', '3', '18');
INSERT INTO sys_role_permission VALUES ('289', '3', '20');
INSERT INTO sys_role_permission VALUES ('456', '2', '1');
INSERT INTO sys_role_permission VALUES ('457', '2', '17');
INSERT INTO sys_role_permission VALUES ('458', '2', '19');
INSERT INTO sys_role_permission VALUES ('459', '2', '27');
INSERT INTO sys_role_permission VALUES ('460', '2', '28');
INSERT INTO sys_role_permission VALUES ('461', '2', '29');
INSERT INTO sys_role_permission VALUES ('462', '2', '30');
INSERT INTO sys_role_permission VALUES ('463', '4', '1');
INSERT INTO sys_role_permission VALUES ('464', '4', '27');
INSERT INTO sys_role_permission VALUES ('465', '4', '28');
INSERT INTO sys_role_permission VALUES ('531', '1', '1');
INSERT INTO sys_role_permission VALUES ('532', '1', '2');
INSERT INTO sys_role_permission VALUES ('533', '1', '3');
INSERT INTO sys_role_permission VALUES ('534', '1', '4');
INSERT INTO sys_role_permission VALUES ('535', '1', '5');
INSERT INTO sys_role_permission VALUES ('536', '1', '16');
INSERT INTO sys_role_permission VALUES ('537', '1', '17');
INSERT INTO sys_role_permission VALUES ('538', '1', '18');
INSERT INTO sys_role_permission VALUES ('539', '1', '19');
INSERT INTO sys_role_permission VALUES ('540', '1', '20');
INSERT INTO sys_role_permission VALUES ('541', '1', '21');
INSERT INTO sys_role_permission VALUES ('542', '1', '22');
INSERT INTO sys_role_permission VALUES ('543', '1', '27');
INSERT INTO sys_role_permission VALUES ('544', '1', '28');
INSERT INTO sys_role_permission VALUES ('545', '1', '29');
INSERT INTO sys_role_permission VALUES ('546', '1', '30');
INSERT INTO sys_role_permission VALUES ('547', '1', '31');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS sys_user;
CREATE TABLE sys_user (
  userId int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  name varchar(255) DEFAULT NULL COMMENT '姓名',
  sex varchar(255) DEFAULT NULL COMMENT '性别',
  adress varchar(255) DEFAULT NULL COMMENT '地址',
  tel varchar(255) DEFAULT NULL COMMENT '电话',
  userName varchar(255) DEFAULT NULL COMMENT '登录名',
  password varchar(255) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (userId)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO sys_user VALUES ('16', '杨金山', '男', '重庆市', '12345678901', 'yjs', 'yjs');
INSERT INTO sys_user VALUES ('17', '李鑫', '男', '重庆市', '09876543212', 'lx', 'lx');
INSERT INTO sys_user VALUES ('18', '刘青华', '男', '重庆市', '123132131312', 'lqh', 'lqh');
INSERT INTO sys_user VALUES ('19', '钟志雄', '男', '重庆市', '123213213', 'zzx', 'zzx');
INSERT INTO sys_user VALUES ('20', '邱巡', '男', '重庆市', '32132132', 'qx', 'qx');
INSERT INTO sys_user VALUES ('21', '杨鹏飞', '男', '重庆市', '21321321', 'ypf', 'ypf');

-- ----------------------------
-- Table structure for sys_user2
-- ----------------------------
DROP TABLE IF EXISTS sys_user2;
CREATE TABLE sys_user2 (
  userId int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  name varchar(255) DEFAULT NULL COMMENT '姓名',
  sex varchar(255) DEFAULT NULL COMMENT '性别',
  adress varchar(255) DEFAULT NULL COMMENT '地址',
  tel varchar(255) DEFAULT NULL COMMENT '电话',
  userName varchar(255) DEFAULT NULL COMMENT '登录名',
  password varchar(255) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (userId)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of sys_user2
-- ----------------------------
INSERT INTO sys_user2 VALUES ('1', '潘大爷', '男', '重庆', '213', 'admin', '111');
INSERT INTO sys_user2 VALUES ('2', '陶金华', '男', '成都', '231', 'tjh', '111');
INSERT INTO sys_user2 VALUES ('3', '聂学生', '女', '重庆', '11212', 'nxs', '111');
INSERT INTO sys_user2 VALUES ('4', '潘大爷', '男', '重庆', '213', 'admin', '111');
INSERT INTO sys_user2 VALUES ('5', '陶金华', '男', '成都', '231', 'tjh', '111');
INSERT INTO sys_user2 VALUES ('6', '聂学生', '女', '重庆', '11212', 'nxs', '111');
INSERT INTO sys_user2 VALUES ('7', '潘大爷', '男', '重庆', '213', 'admin', '111');
INSERT INTO sys_user2 VALUES ('8', '陶金华', '男', '成都', '231', 'tjh', '111');
INSERT INTO sys_user2 VALUES ('9', '聂学生', '女', '重庆', '11212', 'nxs', '111');
INSERT INTO sys_user2 VALUES ('10', '潘大爷', '男', '重庆', '213', 'admin', '111');
INSERT INTO sys_user2 VALUES ('11', '陶金华', '男', '成都', '231', 'tjh', '111');
INSERT INTO sys_user2 VALUES ('12', '聂学生', '女', '重庆', '11212', 'nxs', '111');
INSERT INTO sys_user2 VALUES ('13', '潘大爷', '男', '重庆', '213', 'admin', '111');
INSERT INTO sys_user2 VALUES ('14', '陶金华', '男', '成都', '231', 'tjh', '111');
INSERT INTO sys_user2 VALUES ('15', '聂学生', '女', '重庆', '11212', 'nxs', '111');
INSERT INTO sys_user2 VALUES ('16', '潘大爷', '男', '重庆', '213', 'admin', '111');
INSERT INTO sys_user2 VALUES ('17', '陶金华', '男', '成都', '231', 'tjh', '111');
INSERT INTO sys_user2 VALUES ('18', '聂学生', '女', '重庆', '11212', 'nxs', '111');
INSERT INTO sys_user2 VALUES ('19', '潘大爷', '男', '重庆', '213', 'admin', '111');
INSERT INTO sys_user2 VALUES ('20', '陶金华', '男', '成都', '231', 'tjh', '111');
INSERT INTO sys_user2 VALUES ('21', '聂学生', '女', '重庆', '11212', 'nxs', '111');
INSERT INTO sys_user2 VALUES ('22', '潘大爷', '男', '重庆', '213', 'admin', '111');
INSERT INTO sys_user2 VALUES ('23', '陶金华', '男', '成都', '231', 'tjh', '111');
INSERT INTO sys_user2 VALUES ('24', '聂学生', '女', '重庆', '11212', 'nxs', '111');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS sys_user_role;
CREATE TABLE sys_user_role (
  userRoleId int(11) NOT NULL AUTO_INCREMENT,
  userId int(11) DEFAULT NULL COMMENT '用户ID',
  roleId int(11) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (userRoleId)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COMMENT='用户角色';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO sys_user_role VALUES ('29', '13', '2');
INSERT INTO sys_user_role VALUES ('30', '12', '1');
INSERT INTO sys_user_role VALUES ('32', '14', '3');
INSERT INTO sys_user_role VALUES ('33', '15', '4');
INSERT INTO sys_user_role VALUES ('34', '16', '2');
INSERT INTO sys_user_role VALUES ('35', '17', '1');
INSERT INTO sys_user_role VALUES ('36', '18', '4');
INSERT INTO sys_user_role VALUES ('37', '19', '3');
INSERT INTO sys_user_role VALUES ('38', '20', '1');
INSERT INTO sys_user_role VALUES ('39', '21', '1');
